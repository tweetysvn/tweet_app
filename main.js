var app = new Vue({
    el: '#app',
    data: {
        loading: false,
        maxChars: 140,
        minChars: 2,
        newTweet: '',
        tweets: [
            'This is the next tweet',
            'This is the second tweet',
            'This is the first tweet',
        ],
        showModal: false,
    },
    computed: {
        characterTeller() {
            var char = this.newTweet.length
            var limit = this.maxChars
            return limit - char
        },
        inputLength() {
            return this.newTweet.length > this.maxChars || this.newTweet.length < this.minChars
        },

    },
    methods: {
        toggleShowModal() {
            this.showModal = !this.showModal
            this.newTweet = ''
        },

        addTweet() {
            this.loading = true
            this.tweets.unshift(this.newTweet)
            setTimeout(() => {
                this.loading = false
            }, 2000),
                setTimeout(() => {
                    this.showModal = false
                }, 2000)
        },
        deleteTweet(tweet) {
            var index = this.tweets.indexOf(tweet);
            this.tweets.splice(index, 1);
            this.tweet = false
        }

    }
});